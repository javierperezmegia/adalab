import React from 'react';
import PropTypes from 'prop-types';
import withAuthorization from '../auth/withAuthorization';
import CandidatesTable from './candidatesTable';

const HomePage = ({ authUser }) => {

  return (
    <div>
      <p>{authUser.email}!</p>
      <CandidatesTable />
    </div>
  );
};

HomePage.propTypes = {
  authUser: PropTypes.object.isRequired,
};

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(HomePage);
