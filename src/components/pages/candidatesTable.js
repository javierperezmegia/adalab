import React from 'react';
import { db } from "../../firebase/firebase";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class CandidatesTable extends React.Component {
    constructor() {
        super();
        this.ref = db.collection('candidatas');
        this.unsubscribe = null;
        this.state = {
            candidates: []
        };
        console.log(this.ref);
    }

    onCollectionUpdate = (querySnapshot) => {
        const candidates = [];
        querySnapshot.forEach((doc) => {
          const { nombre_completo, email, codigo_postal, fecha_nacimiento, edad } = doc.data();
          candidates.push({
            key: doc.id,
            nombre_completo,
            email,
            codigo_postal,
            fecha_nacimiento,
            edad
          });
        });
        this.setState({
          candidates
       });
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    }

    render() {
        return (
            <Paper>
                <Table>
                <TableHead>
                    <TableRow>
                    <TableCell>Nombre completo</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Código postal</TableCell>
                    <TableCell>Fecha de nacimiento</TableCell>
                    <TableCell>Edad</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.state.candidates.map(candidate => (
                    <TableRow>
                        <TableCell>{candidate.nombre_completo}</TableCell>
                        <TableCell>{candidate.email}</TableCell>
                        <TableCell>{candidate.codigo_postal}</TableCell>
                        <TableCell>{candidate.fecha_nacimiento}</TableCell>
                        <TableCell>{candidate.edad}</TableCell>
                    </TableRow>
                    ))}
                </TableBody>
                </Table>
            </Paper>
        );
    }
}

export default CandidatesTable;