'use strict';

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Cloud Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

function calculateAge(birth_date) {

    let now = new Date();
    let dateParts = birth_date.split(" ");
    dateParts = dateParts[0].split("/");
    let candidateAge = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);

    // Si ha llegado su fecha de cumpleaños cogemos la diferencia de años, y si no restamos uno
    if (now.getMonth() >= candidateAge.getMonth() && now.getDate() >= candidateAge.getDate()) {
        return now.getFullYear() - candidateAge.getFullYear()
    } else  {
        return now.getFullYear() - candidateAge.getFullYear() - 1
    }

}

exports.addCandidate = functions.https.onRequest(async (req, res) => {

    // Take the data
    const data = req.body;

    // Aunque google form hace las validaciones de los campos no sé en manos de quien puede acabar la URL 
    // de la cloud function asique valido que los datos estén dentro de unos parámetros normales para que no nos
    // puedan hacer fechorías como introducirnos valores gigantes haciendonos asi perder dinero con las cuotas.
    // Y ya de paso compruebo que estén los campos que necesitamos.
    const candidata = {
        nombre_completo: data.nombre_completo || "",
        email: data.email || "",
        codigo_postal: data.codigo_postal || 0,
        fecha_nacimiento: data.fecha_nacimiento || "",
        edad: calculateAge(data.fecha_nacimiento) || 0
    }

    let nombreBien = (candidata.nombre_completo.lenght > 0 && candidata.nombre_completo.lenght < 100) ? true : false;
    let emailBien = (candidata.email.lenght > 0 && candidata.email.lenght < 100) ? true : false;
    let postalBien = (candidata.codigo_postal > 999 && candidata.codigo_postal < 53000) ? true : false;
    let edadBien = (candidata.edad > 17 && candidata.edad < 39) ? true : false;

    // Si nada ha hecho saltar la alarma procedemos a su ingreso en la BBDD, de lo contrario simplemente devolvemos un error
    if (nombreBien && emailBien && postalBien && edadBien) {
        // Push the new message into Cloud Firestore using the Firebase Admin SDK.
        const writeResult = await admin.firestore().collection('candidatas').add(candidata);
    
        // Send back a message that we've succesfully written the message
        res.json({result: `Message with ID: ${writeResult.id} added.`});
    } else {
        // Send back a message that we've succesfully written the message
        res.json({result: `There was a problem with your request.`, nombre: nombreBien, email: emailBien, postal: postalBien, edad: edadBien});
    }

});
